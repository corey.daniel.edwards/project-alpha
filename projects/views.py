from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from projects.models import Project
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin


class list_projects(LoginRequiredMixin, ListView):
    model = Project
    template_name = "list_projects.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(DetailView):
    model = Project
    template_name = "show_project.html"


class ProjectCreateView(CreateView):
    model = Project
    fields = ["name", "description", "members"]
    template_name = "create_project.html"
    success_url = reverse_lazy("home")

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)
