from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from tasks.models import Task
from django.urls import reverse_lazy
from projects.models import Project


class TaskCreateView(CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    template_name = "create_task.html"
    success_url = reverse_lazy("home")

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class TaskListView(ListView):
    model = Task
    template_name = "show_my_tasks.html"


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]
    template_name = "show_my_tasks.html"
    success_url = reverse_lazy("show_my_tasks")
